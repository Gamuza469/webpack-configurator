const codeTranspilation = require('./codeTranspilation');
const styleProcessing = require('./styleProcessing');

module.exports = {
    ...codeTranspilation,
    ...styleProcessing
};
