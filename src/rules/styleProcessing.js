const autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

/**
 * Extract CSS from JS files then load them.
 * Basic combo from mini-css-extract-plugin and css-loader.
 * 
 * Returns an array with both loaders to be used by the rule.
 * 
 * Dependencies: css-loader
 */
const loadCssFiles = () => [
    MiniCssExtractPlugin.loader,
    'css-loader'
];

/**
 * Appends the browser-specific CSS prefixes after loading the CSS files.
 * 
 * Returns an object.
 * 
 * Dependencies: postcss-loader
 */
const addVendorPrefixes = () => ({
    loader: 'postcss-loader',
    options: {
        plugins: () => [autoprefixer()]
    }
});

/**
 * Load SASS files. Do this preferrably after loading CSS files.
 * 
 * Returns an object.
 * 
 * Dependencies: sass-loader
 */
const loadSassFiles = () => ({
    loader: 'sass-loader',
    options: {
        sassOptions: {
            includePaths: ['./node_modules']
        }
    }
});

/**
 * Load and process all style files (CSS/SASS/SCSS files).
 * 
 * Returns an object.
 * 
 * Dependencies: css-loader, postcss-loader, sass-loader.
 */
const processStyleFiles = () => ({
    test: /\.(sa|sc|c)ss$/,
    use: [
        ...loadCssFiles(),
        addVendorPrefixes(),
        loadSassFiles(),
    ]
});

module.exports = {
    addVendorPrefixes,
    loadCssFiles,
    loadSassFiles,
    processStyleFiles
};
