/**
 * Transpile all project files to ES5.
 * This does not include /node_modules folder which is the default here.
 * 
 * Dependencies: babel-loader
 */
const transpileCodeFiles = () => ({
    test: /\.js$/,
    exclude: /node_modules/,
	use: 'babel-loader'
});

module.exports = {
    transpileCodeFiles
};
