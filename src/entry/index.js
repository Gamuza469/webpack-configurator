const addReactHotLoader = (appEntry) => [
    'react-hot-loader/patch',
    'webpack-hot-middleware/client',
    appEntry
];

module.exports = {
    addReactHotLoader
};
