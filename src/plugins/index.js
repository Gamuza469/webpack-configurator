const MiniCssExtractPlugin = require("mini-css-extract-plugin");

/**
 * If using the mini-css-extract-plugin then use this method to create the CSS bundle file.
 * 
 * Returns an object.
 */
const defineCssBundle = (config) => {
    let options = config;

    if (typeof config === 'string') {
        options = {
            filename: config
        };
    }

    return new MiniCssExtractPlugin(options);
};

module.exports = {
    defineCssBundle
};
