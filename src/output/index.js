const path = require('path');

/**
 * Set the path for outputting the bundles.
 * The path will be relative to the project's root directory.
 * 
 * Returns a string.
 */
const setProjectOutputPath = projectPath => path.resolve(process.cwd(), projectPath);

module.exports = {
    setProjectOutputPath
};
