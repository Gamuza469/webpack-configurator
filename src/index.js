const entry = require('./entry')
const output = require('./output');
const rules = require('./rules');
const plugins = require('./plugins');

module.exports = {
    ...entry,
    ...output,
    ...rules,
    ...plugins
};
